package com.avialdo.autotext.abstracts;

import android.support.v4.app.Fragment;
import android.util.Log;
import android.widget.Toast;

import com.avialdo.autotext.util.SQLHelper;


public abstract class AbstractFragment extends Fragment {

	protected SQLHelper db;

	public void showToast(String text, int duration) {

		Toast.makeText(this.getActivity(), text, duration).show();
	}

	public void longLog(String str) {
		if (str.length() > 4000) {
			Log.e("Response", str.substring(0, 4000));
			longLog(str.substring(4000));
		} else
			Log.e("Response", str);
	}
}
