package com.avialdo.autotext.fragment;

import android.Manifest;
import android.app.Activity;
import android.content.ContentResolver;
import android.content.ContentUris;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.provider.MediaStore;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.util.*;
import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.avialdo.autotext.R;
import com.avialdo.autotext.abstracts.AWFragment;
import com.avialdo.autotext.activity.Dashboard;
import com.avialdo.autotext.util.ImgUtil;
import com.avialdo.autotext.util.SQLHelper;
import com.makeramen.roundedimageview.RoundedImageView;

import java.io.ByteArrayOutputStream;
import java.io.InputStream;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;


/**
 * Created by wahabjawed on 29/03/2016.
 */
public class AddContact extends AWFragment {


    @Bind(R.id.img)
    RoundedImageView img;
    @Bind(R.id.edit)
    ImageView edit;
    @Bind(R.id.name)
    EditText name;
    @Bind(R.id.number)
    EditText number;
    @Bind(R.id.imports)
    TextView imports;
    @Bind(R.id.next)
    TextView next;

    private int CAMERA_REQUEST = 1888;
    private int RESULT_LOAD_IMAGE = 1;
    byte[] byteArray;
    private final int PICK_CONTACT = 3;
    private final int MY_PERMISSIONS_REQUEST_CONTACT = 100;


    private String contactId;
    InputStream input = null;
    boolean hasImage = false;
    int ID;
    boolean isUpdate = false;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.add_contact, container, false);
        ((Dashboard) getActivity()).mTitle.setText("Contacts");
        ButterKnife.bind(this, view);
        setupView(view);
        setupListener();

        return view;
    }

    @Override
    public void setupView(View layoutView) {

    }

    @Override
    public void setupListener() {

        img.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub

                registerForContextMenu(img);
                getActivity().openContextMenu(img);


            }
        });


    }


    @Override
    public void onDestroyView() {
        super.onDestroyView();
        ButterKnife.unbind(this);
    }

    @OnClick({R.id.imports, R.id.next})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.imports:


                if (ContextCompat.checkSelfPermission(AddContact.this.getActivity(),
                        Manifest.permission.READ_CONTACTS)
                        != PackageManager.PERMISSION_GRANTED) {

                    // Should we show an explanation?
                    if (ActivityCompat.shouldShowRequestPermissionRationale(AddContact.this.getActivity(),
                            Manifest.permission.READ_CONTACTS)) {

                        // Show an expanation to the user *asynchronously* -- don't block
                        // this thread waiting for the user's response! After the user
                        // sees the explanation, try again to request the permission.
                        ActivityCompat.requestPermissions(AddContact.this.getActivity(),
                                new String[]{Manifest.permission.READ_CONTACTS
                                },
                                MY_PERMISSIONS_REQUEST_CONTACT);


                    } else {

                        // No explanation needed, we can request the permission.

                        ActivityCompat.requestPermissions(AddContact.this.getActivity(),
                                new String[]{Manifest.permission.READ_CONTACTS,
                                },
                                MY_PERMISSIONS_REQUEST_CONTACT);

                        // MY_PERMISSIONS_REQUEST_READ_CONTACTS is an
                        // app-defined int constant. The callback method gets the
                        // result of the request.
                    }
                } else {

                    Intent intent = new Intent(Intent.ACTION_PICK,
                            ContactsContract.Contacts.CONTENT_URI);
                    startActivityForResult(intent, PICK_CONTACT);
                }


                break;
            case R.id.next:


                if (name.getText().toString() == ""
                        && number.getText().toString() == "") {

                    showToast("Kindly Enter a Valid Name and Number",
                            Toast.LENGTH_SHORT);
                } else if (name.getText().toString() == "") {

                    showToast(
                            "Kindly Enter a Valid Name", Toast.LENGTH_SHORT);

                } else if (number.getText().toString() == "") {

                    showToast(
                            "Kindly Enter a Valid Number", Toast.LENGTH_SHORT);

                } else if (number.getText().toString().length() < 9) {

                    showToast(
                            "Kindly Enter a Valid Number", Toast.LENGTH_SHORT);

                } else {
                    if (hasImage) {

                        ByteArrayOutputStream stream = new ByteArrayOutputStream();
                        (((BitmapDrawable) img.getDrawable())
                                .getBitmap()).compress(
                                Bitmap.CompressFormat.PNG, 10, stream);
                        byte[] byteArray = stream.toByteArray();
                        if (!isUpdate) {
                            SQLHelper.insertContact(name.getText().toString(),
                                    number.getText().toString(), byteArray);
                        } else {
                            SQLHelper.updateContact(ID, name.getText()
                                            .toString(), number.getText().toString(),
                                    byteArray);
                        }
                    } else {

                        if (!isUpdate) {
                            SQLHelper.insertContact(name.getText().toString(),
                                    number.getText().toString(), null);
                        } else {
                            SQLHelper.updateContact(ID, name.getText()
                                            .toString(), number.getText().toString(),
                                    null);
                        }
                    }

                }

                break;
        }
    }


    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        super.onCreateContextMenu(menu, v, menuInfo);

        if (v.getId() == R.id.img) {
            menu.add(0, v.getId(), 0, "Take Photo");
            menu.add(0, v.getId(), 1, "Select From Gallery");
        }

    }

    @Override
    public boolean onContextItemSelected(MenuItem item) {

        if (item.getTitle() == "Take Photo") {


            Intent cameraIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
            startActivityForResult(cameraIntent, CAMERA_REQUEST);

        } else if (item.getTitle() == "Select From Gallery") {

            Intent i = new Intent(
                    Intent.ACTION_PICK,
                    MediaStore.Images.Media.EXTERNAL_CONTENT_URI);

            startActivityForResult(i, RESULT_LOAD_IMAGE);

        }

        return super.onContextItemSelected(item);

    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == CAMERA_REQUEST && resultCode == Activity.RESULT_OK) {
            Bitmap photo = (Bitmap) data.getExtras().get("data");
            img.setImageBitmap(photo);

            ByteArrayOutputStream stream = new ByteArrayOutputStream();
            assert photo != null;
            photo.compress(Bitmap.CompressFormat.PNG, 100, stream);
            byteArray = stream.toByteArray();


        } else if (requestCode == RESULT_LOAD_IMAGE && resultCode == Activity.RESULT_OK && null != data) {

            Bitmap bitmap = ImgUtil.decodeUri(data.getData());
            img.setImageBitmap(bitmap);

            ByteArrayOutputStream stream = new ByteArrayOutputStream();
            assert bitmap != null;
            bitmap.compress(Bitmap.CompressFormat.PNG, 100, stream);
            byteArray = stream.toByteArray();
        } else if (requestCode == PICK_CONTACT && resultCode == Activity.RESULT_OK) {
            Uri contactData = data.getData();
            ContentResolver cr = getActivity().getContentResolver();
            name.setText(null);
            number.setText(null);

            @SuppressWarnings("deprecation")
            Cursor c = getActivity().managedQuery(contactData, null, null, null, null);
            if (c.moveToFirst()) {

                if (c.getColumnIndex(ContactsContract.Contacts._ID) != -1) {
                    contactId = c.getString(c
                            .getColumnIndex(ContactsContract.Contacts._ID));

                    if (c.getColumnIndex(ContactsContract.Contacts.DISPLAY_NAME) != -1) {
                        android.util.Log.d("name",
                                ""
                                        + c.getColumnIndex(ContactsContract.Contacts.DISPLAY_NAME));
                        name.setText(c.getString(c
                                .getColumnIndex(ContactsContract.Contacts.DISPLAY_NAME)));
                    }

                    Cursor phones = cr.query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI, null,
                            ContactsContract.CommonDataKinds.Phone.CONTACT_ID + " = " + contactId, null,
                            null);
                    if (phones.moveToFirst()) {
                        if (phones
                                .getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER) != -1) {
                            {
                                android.util.Log.d("number",
                                        ""
                                                + phones.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER));
                                number.setText(phones.getString(phones
                                        .getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER)));
                            }

                        }
                    }
                    Uri uri = ContentUris.withAppendedId(
                            ContactsContract.Contacts.CONTENT_URI,
                            Long.parseLong(contactId));
                    input = ContactsContract.Contacts
                            .openContactPhotoInputStream(cr, uri);
                    if (input != null) {
                        img.setImageBitmap(BitmapFactory
                                .decodeStream(input));
                        hasImage = true;

                    } else {
                        img.setImageResource(R.drawable.photo);
                        hasImage = false;
                    }

                }

            }

        }

    }


    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        switch (requestCode) {

            case MY_PERMISSIONS_REQUEST_CONTACT: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    Intent intent = new Intent(Intent.ACTION_PICK,
                            ContactsContract.Contacts.CONTENT_URI);
                    startActivityForResult(intent, PICK_CONTACT);

                } else {

                    showToast("Permission Required", Toast.LENGTH_SHORT);
                }
                return;
            }
        }


    }


}


