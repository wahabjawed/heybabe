package com.avialdo.autotext.fragment;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.avialdo.autotext.R;
import com.avialdo.autotext.abstracts.AWFragment;


/**
 * Created by wahabjawed on 29/03/2016.
 */
public class Setting extends AWFragment {

    public Setting() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment, container, false);
    }

    @Override
    public void setupView(View layoutView) {

    }

    @Override
    public void setupListener() {

    }


}


