package com.avialdo.autotext.util;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.provider.Settings;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.TaskStackBuilder;

import com.avialdo.autotext.AppC;
import com.avialdo.autotext.R;


public class Notification {

	public static  void NewMessageProgressNotification(String message, String title,int id, boolean state) {
		// Creates an explicit intent for an Activity in your app
		//Intent resultIntent = new Intent(AppC.context, null);

		// The stack builder object will contain an artificial back
		// stack for the
		// started Activity.
		// This ensures that navigating backward from the Activity leads
		// out of
		// your application to the Home screen.
		TaskStackBuilder stackBuilder = TaskStackBuilder
				.create(AppC.context);
		// Adds the back stack for the Intent (but not the Intent
		// itself)
		//stackBuilder.addParentStack(AppC.class);
		// Adds the Intent that starts the Activity to the top of the
		// stack
		//stackBuilder.addNextIntent(resultIntent);
		PendingIntent resultPendingIntent = PendingIntent.getActivity(
				AppC.context,
				0,
				new Intent(), // add this
				PendingIntent.FLAG_UPDATE_CURRENT);
		NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(
				AppC.context).setSmallIcon(R.drawable.logo)
				.setContentTitle(title).setContentIntent(resultPendingIntent)
				.setVibrate(new long[]{100, 250, 100, 500})
				.setSound(Settings.System.DEFAULT_NOTIFICATION_URI)
				.setContentText(message).setAutoCancel(true);

		int notifyID = id;
		// mBuilder.setContentIntent(resultPendingIntent);
		NotificationManager mNotificationManager = (NotificationManager) AppC.context
				.getSystemService(Context.NOTIFICATION_SERVICE);

		// mId allows you to update the notification later on.
		mBuilder.setProgress(0, 0, state);
		mNotificationManager.notify(notifyID, mBuilder.build());

	}
}
