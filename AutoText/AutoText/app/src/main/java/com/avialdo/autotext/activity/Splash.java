package com.avialdo.autotext.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Handler;

import com.avialdo.autotext.AppC;
import com.avialdo.autotext.R;
import com.avialdo.autotext.abstracts.AWActivity;


public class Splash extends AWActivity {


    static Context context = AppC.context;
    final Handler handler = new Handler();
    Runnable r = null;

    @Override
    public void setupView() {
        // TODO Auto-generated method stub
        setContentView(R.layout.activity_splash);

        final Handler handler = new Handler();
        r = new Runnable() {
            public void run() {
                Intent i = new Intent(Splash.this, Dashboard.class);
                startActivity(i);
                Splash.this.finish();
            }

        };


        handler.postDelayed(r, 2000);


    }

    @Override
    public void setupListener() {

    }


}
