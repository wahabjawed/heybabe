package com.avialdo.autotext.abstracts;

import android.support.v4.app.FragmentActivity;
import android.widget.Toast;

import com.avialdo.autotext.util.SQLHelper;


public abstract class AbstractFragmentActivity extends FragmentActivity {

	protected SQLHelper db;

	public void showToast(String text, int duration) {

		Toast.makeText(this, text, duration).show();
	}
}
