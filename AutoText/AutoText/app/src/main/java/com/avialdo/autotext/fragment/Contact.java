package com.avialdo.autotext.fragment;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.database.Cursor;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.TextView;

import com.avialdo.autotext.R;
import com.avialdo.autotext.abstracts.AWFragment;
import com.avialdo.autotext.activity.Dashboard;
import com.avialdo.autotext.adapter.ContactAdapter;
import com.avialdo.autotext.model.ContactObject;
import com.avialdo.autotext.util.SQLHelper;
import com.nhaarman.listviewanimations.appearance.simple.AlphaInAnimationAdapter;

import java.util.ArrayList;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;


/**
 * Created by wahabjawed on 29/03/2016.
 */
public class Contact extends AWFragment {

    @Bind(R.id.contact_list)
    ListView contactList;
    @Bind(R.id.emptyElement)
    TextView emptyElement;
    @Bind(R.id.done)
    FloatingActionButton done;

    public ArrayList<ContactObject> listObj = null;
    private AlphaInAnimationAdapter mAnimAdapter;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        listObj = ((Dashboard) getActivity()).contactsList;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.contact, container, false);
        ((Dashboard) getActivity()).mTitle.setText("Contacts");
        ButterKnife.bind(this, view);
        setupView(view);
        setupListener();

        return view;
    }


    @Override
    public void onResume() {
        super.onResume();

        if (((Dashboard) getActivity()).bottomtab.getVisibility() == View.GONE) {
            ((Dashboard) getActivity()).bottomtab.setVisibility(View.VISIBLE);
            ((Dashboard) getActivity()).bottomtab.animate()
                    .translationY(0).setDuration(500)
                    .setListener(new AnimatorListenerAdapter() {
                        @Override
                        public void onAnimationEnd(Animator animation) {
                            super.onAnimationEnd(animation);
                            dataProcessing();
                        }
                    });
        } else {
            dataProcessing();
        }
    }

    @Override
    public void setupView(View layoutView) {


        contactList.setEmptyView(emptyElement);


    }

    @Override
    public void setupListener() {

    }


    private void dataProcessing() {
        if (listObj.size() == 0) {
            fetchContacts();
        }
        ContactAdapter adapter = new ContactAdapter(this, listObj);
        mAnimAdapter = new AlphaInAnimationAdapter(adapter);
        mAnimAdapter.setAbsListView(contactList);
        contactList.setAdapter(mAnimAdapter);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        ButterKnife.unbind(this);
    }

    @OnClick(R.id.done)
    public void onClick() {


        final FragmentTransaction fragmentTransaction = ((Dashboard) getActivity()).fragmentManager.beginTransaction();
        fragmentTransaction.addToBackStack(null);
        fragmentTransaction.setCustomAnimations(R.anim.enter_from_right, R.anim.exit_to_left, R.anim.enter_from_left, R.anim.exit_to_right);
        ((Dashboard) getActivity()).bottomtab.animate()
                .translationY(((Dashboard) getActivity()).bottomtab.getHeight())
                .setDuration(500)
                .setListener(new AnimatorListenerAdapter() {
                    @Override
                    public void onAnimationEnd(Animator animation) {
                        super.onAnimationEnd(animation);
                        ((Dashboard) getActivity()).fragment = new AddContact();
                        fragmentTransaction.replace(R.id.container_body, new AddContact());
                        fragmentTransaction.commit();
                        ((Dashboard) getActivity()).bottomtab.setVisibility(View.GONE);
                    }
                });

    }


    private void fetchContacts() {
        Cursor _cursor = SQLHelper.getDashboardContactList();
        listObj.clear();

        for (int i = 0; i < _cursor.getCount(); i++) {
            _cursor.moveToFirst();
            listObj.add(new ContactObject(_cursor.getInt(_cursor
                    .getColumnIndex("ID")), _cursor.getString(_cursor
                    .getColumnIndex("name")), _cursor.getString(_cursor
                    .getColumnIndex("number")), _cursor.getBlob(_cursor
                    .getColumnIndex("displayPic"))));

            _cursor.moveToNext();
        }
        _cursor.close();

    }


}


