package com.avialdo.autotext.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.avialdo.autotext.R;
import com.avialdo.autotext.abstracts.AWActivity;
import com.avialdo.autotext.fragment.Contact;
import com.avialdo.autotext.fragment.General;
import com.avialdo.autotext.fragment.Home;
import com.avialdo.autotext.fragment.Log;
import com.avialdo.autotext.fragment.Setting;
import com.avialdo.autotext.model.ContactObject;

import java.util.ArrayList;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by Wahab on 03/18/16.
 */
public class Dashboard extends AWActivity {


    public TextView mTitle;
    int fragmentNo = 1;
    public Toolbar mToolbar;
    public Fragment fragment = null;
    @Bind(R.id.container_body)
    FrameLayout containerBody;
    @Bind(R.id.generalI)
    ImageView generalI;
    @Bind(R.id.general)
    LinearLayout general;
    @Bind(R.id.contactI)
    ImageView contactI;
    @Bind(R.id.contact)
    LinearLayout contact;
    @Bind(R.id.homeI)
    ImageView homeI;
    @Bind(R.id.home)
    LinearLayout home;
    @Bind(R.id.logsI)
    ImageView logsI;
    @Bind(R.id.logs)
    LinearLayout logs;
    @Bind(R.id.settingI)
    ImageView settingI;
    @Bind(R.id.setting)
    LinearLayout setting;
    @Bind(R.id.bottomtab)
    public LinearLayout bottomtab;


    public  ArrayList<ContactObject> contactsList = new ArrayList<ContactObject>();
    public  FragmentManager fragmentManager;

    @Override
    public void setupView() {


//        if (AppC.prefs.getBoolean("first_time", false) == false) {
//            startActivity(new Intent(this, Splash.class));
//            this.finish();
//            return;
//        }


        setContentView(R.layout.dashboard);
        ButterKnife.bind(this);

        mToolbar = (Toolbar) findViewById(R.id.toolbar);

        mTitle = (TextView) mToolbar.findViewById(R.id.toolbar_title);
        setSupportActionBar(mToolbar);

        getSupportActionBar().setDisplayShowTitleEnabled(false);

        fragmentManager = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.replace(containerBody.getId(), new Home());
        fragmentTransaction.commit();


    }

    @Override
    public void setupListener() {

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        fragment.onActivityResult(requestCode, resultCode, data);

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // TODO: add setContentView(...) invocation

    }


    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {

        fragment.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }

    @OnClick({R.id.general, R.id.contact, R.id.home, R.id.logs, R.id.setting})
    public void onClick(View view) {
        fragmentManager = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        switch (view.getId()) {
            case R.id.general:
                if (fragmentNo != 1) {
                    fragmentNo = 1;
                    fragmentTransaction.setCustomAnimations(R.anim.enter_from_left, R.anim.exit_to_right);
                    fragmentTransaction.replace(containerBody.getId(), new General());
                    fragmentTransaction.commit();
                }
                break;
            case R.id.contact:
                if (fragmentNo != 2) {
                    if (fragmentNo > 2) {
                        fragmentTransaction.setCustomAnimations(R.anim.enter_from_left, R.anim.exit_to_right);
                    } else {
                        fragmentTransaction.setCustomAnimations(R.anim.enter_from_right, R.anim.exit_to_left);
                    }
                    fragmentNo = 2;
                    fragmentTransaction.replace(containerBody.getId(), new Contact());
                    fragmentTransaction.commit();
                }
                break;
            case R.id.home:
                if (fragmentNo != 3) {
                    if (fragmentNo > 3) {
                        fragmentTransaction.setCustomAnimations(R.anim.enter_from_left, R.anim.exit_to_right);
                    } else {
                        fragmentTransaction.setCustomAnimations(R.anim.enter_from_right, R.anim.exit_to_left);
                    }
                    fragmentNo = 3;
                    fragmentTransaction.replace(containerBody.getId(), new Home());
                    fragmentTransaction.commit();
                }
                break;
            case R.id.logs:
                if (fragmentNo != 4) {
                    if (fragmentNo > 4) {
                        fragmentTransaction.setCustomAnimations(R.anim.enter_from_left, R.anim.exit_to_right);
                    } else {
                        fragmentTransaction.setCustomAnimations(R.anim.enter_from_right, R.anim.exit_to_left);
                    }
                    fragmentNo = 4;
                    fragmentTransaction.replace(containerBody.getId(), new Log());
                    fragmentTransaction.commit();
                }
                break;
            case R.id.setting:
                if (fragmentNo != 5) {
                    if (fragmentNo > 5) {
                        fragmentTransaction.setCustomAnimations(R.anim.enter_from_left, R.anim.exit_to_right);
                    } else {
                        fragmentTransaction.setCustomAnimations(R.anim.enter_from_right, R.anim.exit_to_left);
                    }
                    fragmentNo = 5;
                    fragmentTransaction.replace(containerBody.getId(), new Setting());
                    fragmentTransaction.commit();
                }
                break;
        }


    }

}