package com.avialdo.autotext.abstracts;

import android.os.Bundle;


public abstract class AWNetworkActivity extends AbstractActivity
		implements INetwork,ICallBack.IActivityCallBack {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub

		super.onCreate(savedInstanceState);
		setupView();
		setupListener();
	}

}
