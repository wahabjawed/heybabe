package com.avialdo.autotext.abstracts;

import android.view.View;

/**
 * Created by Availdo on 1/9/2016.
 */
public class ICallBack {
    public interface IActivityCallBack {

        void setupView();

        void setupListener();
    }

    public interface IFragmentCallBack {

        void setupView(View layoutView);

        void setupListener();
    }

}
