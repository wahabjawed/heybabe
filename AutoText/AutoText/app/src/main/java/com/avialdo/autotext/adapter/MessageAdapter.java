package com.avialdo.autotext.adapter;

import android.app.Activity;
import android.content.Context;
import android.graphics.Typeface;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.RadioButton;
import android.widget.TextView;


import com.avialdo.autotext.AppC;
import com.avialdo.autotext.R;
import com.avialdo.autotext.abstracts.AWFragment;
import com.avialdo.autotext.model.MessageObject;


public class MessageAdapter extends ArrayAdapter<MessageObject> {

	private AWFragment fragment;
	private MessageObject[] data;
	private RadioButton mSelectedRB;
	private int mSelectedPosition = -1;

	public MessageAdapter(AWFragment context, MessageObject[] _data) {
		super(context.getActivity(), R.layout.row_message, _data);
		this.fragment = context;
		this.data = _data;
	}

	static class ViewHolder {

		public TextView text;
		public RadioButton select;

	}

	@Override
	public View getView(final int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		View vi = convertView;
		final int pos = position;
		if (vi == null) {
			LayoutInflater inflater = (LayoutInflater) fragment.getActivity()
					.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

			Log.d("BrosApp--ContactAdapter", "Inflating Layout");
			vi = inflater.inflate(R.layout.row_message, parent, false);

			ViewHolder viewHolder = new ViewHolder();

			viewHolder.text = (TextView) vi.findViewById(R.id.message);
			viewHolder.select = (RadioButton) vi.findViewById(R.id.select);
			Typeface face = Typeface.createFromAsset(AppC.getContext()
					.getAssets(), "AdobeGothicStd.otf");
			viewHolder.text.setTypeface(face);

			vi.setTag(viewHolder);
		}
		ViewHolder holder = (ViewHolder) vi.getTag();
		MessageObject obj = data[position];
		holder.text.setText(obj.getMessageText());
		mSelectedPosition = (obj.isSelected()) ? position : mSelectedPosition;
		mSelectedRB = (obj.isSelected()) ? (RadioButton) holder.select
				: mSelectedRB;
		data[position].setSelected((obj.isSelected()) ? !data[position]
				.isSelected() : data[position].isSelected());

		if (mSelectedPosition != position) {
			holder.select.setChecked(false);
		} else {
			holder.select.setChecked(true);
			if (mSelectedRB != null && holder.select != mSelectedRB) {
				mSelectedRB = holder.select;
				Log.e("kjh", "hfg1 " + position + " " + mSelectedRB.isChecked());

			}
		}

		holder.select.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub

				if (position != mSelectedPosition && mSelectedRB != null) {
					mSelectedRB.setChecked(false);
					data[pos].setSelected(!data[mSelectedPosition].isSelected());
				}

				mSelectedPosition = position;
				mSelectedRB = (RadioButton) v;
				data[pos].setSelected(!data[pos].isSelected());
				//((NewMessage) activity).selectedMsgID = pos;

			}
		});

		return vi;
	}
}
