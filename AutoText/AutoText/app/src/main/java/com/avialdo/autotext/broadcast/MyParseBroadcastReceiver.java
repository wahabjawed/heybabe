package com.avialdo.autotext.broadcast;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;

import com.avialdo.autotext.R;
import com.parse.ParsePushBroadcastReceiver;


/**
 * Created by Wahab on 03/14/16.
 */

public class MyParseBroadcastReceiver extends ParsePushBroadcastReceiver {

    private final String TAG = MyParseBroadcastReceiver.class.getSimpleName();


    @Override
    protected int getSmallIconId(Context context, Intent intent) {

        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP) {
            return R.drawable.logo;
        } else {
            return R.drawable.logo;
        }

    }

    @Override
    protected Bitmap getLargeIcon(Context context, Intent intent) {
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP) {
            return BitmapFactory.decodeResource(context.getResources(), R.drawable.logo);
        } else {
            return BitmapFactory.decodeResource(context.getResources(), R.drawable.logo);
        }
    }
}
