package com.avialdo.autotext;

import android.app.Application;
import android.content.Context;
import android.content.SharedPreferences;
import android.database.sqlite.SQLiteDatabase;
import android.preference.PreferenceManager;
import android.util.Log;

import com.avialdo.autotext.util.SQLHelper;

import com.parse.Parse;
import com.parse.ParsePush;
import com.parse.SaveCallback;


/**
 * Created by Availdo on 1/9/2016.
 */
public class AppC extends Application {

    // Note: Your consumer key and secret should be obfuscated in your source code before shipping.
    public static SharedPreferences prefs;
    public static SharedPreferences.Editor editor;
    public static SQLiteDatabase db;
    public static Context context;

    SharedPreferences preferences;


    public static SQLiteDatabase getDb() {
        return db;
    }

    public static Context getContext() {
        return context;
    }

    @Override
    public void onCreate() {

        // TODO Auto-generated method stub
        super.onCreate();
        prefs = PreferenceManager.getDefaultSharedPreferences(this);
        editor = prefs.edit();



        db = openOrCreateDatabase("AppC", MODE_PRIVATE, null);

        context = getApplicationContext();

        preferences = PreferenceManager
                .getDefaultSharedPreferences(getContext());

        SQLHelper.SetupDB();

        Parse.initialize(this, "ydmTMKN3ZJ3UtKXrWipMk8Fd4nUfYfCgVJgb92lB",
                "WddesaUPVKyD9H0oMbCikuP0sGR1aqev9HFBjikV");

        ParsePush.subscribeInBackground("", new SaveCallback() {
            @Override
            public void done(com.parse.ParseException e) {
                if (e == null) {
                    Log.e("com.parse.push",
                            "successfully subscribed to the broadcast channel.");
                } else {
                    Log.e("com.parse.push", "failed to subscribe for push", e);
                }
            }
        });

    }


    @Override
    public void onLowMemory() {
        // TODO Auto-generated method stub
        super.onLowMemory();
    }

    @Override
    public void onTerminate() {
        // TODO Auto-generated method stub
        super.onTerminate();
        db.close();
    }
}
