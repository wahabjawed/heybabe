package com.avialdo.autotext.abstracts;

import android.content.Context;
import android.widget.Toast;

import com.avialdo.autotext.AppC;
import com.avialdo.autotext.AppC;
import com.avialdo.autotext.util.SQLHelper;


public abstract class AbstractRequest {

	protected SQLHelper db;
	public Context context = AppC.getContext();
	public String networkAddress = "http://steve-jones.co/iospanic/";

	public void showToast(String text, int duration) {

		Toast.makeText(AppC.getContext(), text, duration).show();
	}
	public abstract void PerformTask();
}
