package com.avialdo.autotext.adapter;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.avialdo.autotext.R;
import com.avialdo.autotext.abstracts.AWFragment;
import com.avialdo.autotext.model.ContactObject;

import java.util.ArrayList;

public class ContactAdapter extends ArrayAdapter<ContactObject> {

    private AWFragment fragment;
    ArrayList<ContactObject> data;

    public ContactAdapter(AWFragment context, ArrayList<ContactObject> _data) {
        super(context.getActivity(), R.layout.row_contact, _data);
        this.fragment = context;
        this.data = _data;
    }

    static class ViewHolder {

        public TextView text;
        public TextView number;
        public ImageView displayPic;

    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        // TODO Auto-generated method stub
        View vi = convertView;
        if (vi == null) {
            LayoutInflater inflater = (LayoutInflater) fragment.getActivity()
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);

            Log.d("BrosApp--ContactAdapter", "Inflating Layout");
            vi = inflater.inflate(R.layout.row_contact, parent, false);

            ViewHolder viewHolder = new ViewHolder();

            viewHolder.text = (TextView) vi.findViewById(R.id.name);
            viewHolder.number = (TextView) vi.findViewById(R.id.timestamp);
            viewHolder.displayPic = (ImageView) vi
                    .findViewById(R.id.list_image);
            vi.setTag(viewHolder);
        }
        ViewHolder holder = (ViewHolder) vi.getTag();
        ContactObject obj = data.get(position);
        if (obj.getDisplayPic() != null) {
            Bitmap bmp = BitmapFactory.decodeByteArray(obj.getDisplayPic(), 0,
                    obj.getDisplayPic().length);
            holder.displayPic.setImageBitmap(bmp);
        } else {
            holder.displayPic.setImageResource(R.drawable.person);

        }
        holder.text.setText(obj.getName());
        holder.number.setText(obj.getNumber());

        return vi;
    }

}
