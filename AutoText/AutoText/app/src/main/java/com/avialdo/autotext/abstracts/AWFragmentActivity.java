package com.avialdo.autotext.abstracts;

import android.os.Bundle;

public abstract class AWFragmentActivity extends AbstractFragmentActivity implements ICallBack.IActivityCallBack {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        super.onCreate(savedInstanceState);
        setupView();
        setupListener();
    }


}
