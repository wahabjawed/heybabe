package com.avialdo.autotext.abstracts;

import android.support.v7.app.AppCompatActivity;
import android.widget.Toast;

import com.avialdo.autotext.AppC;
import com.avialdo.autotext.AppC;
import com.avialdo.autotext.util.SQLHelper;


public abstract class AbstractActivity extends AppCompatActivity {

	protected SQLHelper db;

	public void showToast(String text, int duration) {

		Toast.makeText(AppC.context, text, duration).show();
	}
}
